import { createDefaultRouterContext, InjectedRouterContextProps, RouterContext } from "../router_context";
import { EMPTY, from, Observable } from "rxjs";
import { Observer } from "rxjs/index";
import * as React from "react";
import injectRouterContext from "../hocs/injectRouterContext";

export const createTestRouterContext = (pathObservable?: Observable<string>): RouterContext => {
  const defaultRouterContext = createDefaultRouterContext();
  return {
    ...defaultRouterContext,
    services: {
      path: {
        getCurrentPath: jest.fn(),
        pathObservable: pathObservable || from(EMPTY),
      },
      navigation: {
        navigateToPath: jest.fn()
      }
    }
  };
};

export const contextPassingComponent = (observer: Observer<RouterContext>) =>
  class TestingComponent extends React.Component<InjectedRouterContextProps> {

    constructor(props: InjectedRouterContextProps) {
      super(props);
      observer.next(props.routerContext);
    }


    componentWillUpdate(nextProps: Readonly<InjectedRouterContextProps>, nextState: Readonly<{}>, nextContext: any): void {
      observer.next(nextProps.routerContext);
    }

    render() {
      return (
        <div>
          Testing
        </div>
      )
    }

    componentWillUnmount() {
      observer.complete();
    }
  };

export const createContextAwareComponent = (observer: Observer<RouterContext>) => injectRouterContext(contextPassingComponent(observer));
