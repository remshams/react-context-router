import * as React from "react";
import LazyRoute from "../lazy_route";
import * as TestRenderer from "react-test-renderer";
import { createTestRouterContext } from "../__test_mocks__/router";
import Router from "../router";
import Loading from "../loading";

describe("LazyRoute", () => {

  const testRouterContext = createTestRouterContext();
  const lazyComponentLoader = () => import("../__test_mocks__/lazy_component");

  const TestComponent = () => (
    <Router routerContext={testRouterContext}>
      <LazyRoute loader={lazyComponentLoader}/>
    </Router>
  );

  it("renders lazy loaded component", () => {

    const wrapper = TestRenderer.create(<TestComponent/>);
    const testInstance = wrapper.root;
    console.log(wrapper.toJSON());
    expect(testInstance.findAllByType(Loading).length).toEqual(1);

  });


});
