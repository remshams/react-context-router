# React Router
React Router based on the new Context API

## Features
  * Nested Routes
  * Param pathing
  
## Installation
```sh
npm install react-context-router --save
yarn add react-context-router
```

## Usage
```javascript
const Child = () => (
  <div>Child</div>
);

const Parent = () => (
  <Route path="child" Component={Child}/>
);


const App = () => {

  return (
    <>
      <Router>
        <Route path="parent" Component={Parent}/>
      </Router>
    </>
  );
};
```
