import * as React from "react";
import { InjectedRouterContextProps } from "./router_context";
import injectRouterContext from "./hocs/injectRouterContext";
import { fromNullable, Option } from "fp-ts/lib/Option";
import { areContextElementsInPath, extractPathElements, PathParams, RouteElement } from "../services/path";
import { extractPathParamsOnRouteContext } from "../services/path_params";

export type PathParamsProp = {
  params: PathParams
};

export type RouteProps = InjectedRouterContextProps & {
  path?: string;
  Component: React.ComponentType<PathParamsProp>
}

const isRoutePathInCurrentPath = (path: Option<string>, pathElements: Array<RouteElement>) => path
  .map(extractPathElements)
  .map(contextElements => areContextElementsInPath(contextElements, pathElements))
  .getOrElse(true);

const extractPathParams = (path: Option<string>, pathElements: Array<RouteElement>) => path
  .map(extractPathElements)
  .map(contextElements => extractPathParamsOnRouteContext(contextElements, pathElements))
  .getOrElse({});

const isVisible = (path: Option<string>, pathElements: Array<RouteElement>) => isRoutePathInCurrentPath(path, pathElements);

const Route = (props: RouteProps) => {

  const pathElements = props.routerContext.router.data.pathElements;
  const path = fromNullable(props.path);

  return (
    <>
      { isVisible(path, props.routerContext.router.data.pathElements) && <props.Component params={extractPathParams(path, pathElements)}/> }
    </>
  );

};


export default injectRouterContext(Route);
