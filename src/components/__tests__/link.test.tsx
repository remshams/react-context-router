import * as React from "react";
import { createTestRouterContext } from "../__test_mocks__/router";
import Router from "../router";
import Link from "../link";
import * as TestRenderer from "react-test-renderer";
import { ReactTestInstance } from "react-test-renderer";

describe("Link", () => {

  const testRouterContext = createTestRouterContext();
  const navigateToPathMock = testRouterContext.services.navigation.navigateToPath as jest.Mock;

  const LinkComponent = () =>
    <Router routerContext={testRouterContext}>
      <Link path="test/path">
        <div className="divForTest"> Content</div>
      </Link>
    </Router>;

  it("renders link", () => {

    const wrapper = TestRenderer.create(<LinkComponent />);
    const testInstance = wrapper.root;

    expect(testInstance.findAllByType(Link).length).toBe(1);
    expect(testInstance.findAllByProps({ className: "divForTest" }).length).toBe(1);
  });

  it("navigates to path on click", () => {

    const wrapper = TestRenderer.create(<LinkComponent/>);

    const linkComponent = wrapper.root.findByType(Link).children[0] as ReactTestInstance;
    const linkDiv = linkComponent.children[0] as ReactTestInstance;
    linkDiv.props.onClick({preventDefault: Function.prototype});

    expect(navigateToPathMock).toHaveBeenCalled();
  });


});
