import * as React from "react";
import Router from "../router";
import Route, { PathParamsProp } from "../route"
import { createTestRouterContext } from "../__test_mocks__/router";
import * as TestRenderer from "react-test-renderer";
import { PathParams } from "../../services/path";

describe("Route", () => {

  const testRouterContext = createTestRouterContext();
  const getCurrentPathMock = testRouterContext.services.path.getCurrentPath as jest.Mock;


  const RouteContent = () =>  <div className="content"/>;

  const createRouteContent = (resolve: (params: PathParams) => void) => (params: PathParams) => {
    resolve(params);
    return <RouteContent/>;
  };


  const routeWithPath = (Component: React.ComponentType<PathParamsProp>, path?: string) => () => {

    return (
      <Router routerContext={testRouterContext}>
        <Route path={path} Component={Component}/>
      </Router>
    );
  };

  it("renders Route content when context is not set", () => {
    const TestRoute = routeWithPath(RouteContent);

    const wrapper = TestRenderer.create(<TestRoute/>);
    const testInstance = wrapper.root;
    expect(testInstance.findAllByType(RouteContent).length).toEqual(1);



  });

  it("renders Route content when context is set", () => {

    getCurrentPathMock.mockReturnValue("parent/child");

    const TestRoute = routeWithPath(RouteContent, "parent");

    const wrapper = TestRenderer.create(<TestRoute/>);
    const testInstance = wrapper.root;
    expect(testInstance.findAllByType(RouteContent).length).toEqual(1);

  });

  it("renders Route content when path is not set", () => {

    getCurrentPathMock.mockReturnValue("parent/child");

    const TestRoute = routeWithPath(RouteContent, undefined);

    const wrapper = TestRenderer.create(<TestRoute/>);
    const testInstance = wrapper.root;
    expect(testInstance.findAllByType(RouteContent).length).toEqual(1);

  });

  it("does not render route in case current path is empty string", () => {

    getCurrentPathMock.mockReturnValue("");

    const TestRoute = routeWithPath(RouteContent, "parent/");

    const wrapper = TestRenderer.create(<TestRoute/>);
    const testInstance = wrapper.root;
    expect(testInstance.findAllByType(RouteContent).length).toEqual(0);

  });

  it("does not render route in case current path is undefined", () => {

    getCurrentPathMock.mockReturnValue(undefined);

    const TestRoute = routeWithPath(RouteContent, "parent/");

    const wrapper = TestRenderer.create(<TestRoute/>);
    const testInstance = wrapper.root;
    expect(testInstance.findAllByType(RouteContent).length).toEqual(0);

  });

  it("does not render Route content when context is not found in current path", () => {

    getCurrentPathMock.mockReturnValue("parent/child");

    const TestRoute = routeWithPath(RouteContent, "doesNotExist");
    const wrapper = TestRenderer.create(<TestRoute/>);
    const testInstance = wrapper.root;
    expect(testInstance.findAllByType(RouteContent).length).toEqual(0);

  });

  it("passes path elements to rendered component", async () => {

    getCurrentPathMock.mockReturnValue("parent/1");

    const paramsReceived = await new Promise(resolve => {

      const ParamsReceivingRouteContent = createRouteContent(resolve);

      const TestRoute = routeWithPath(ParamsReceivingRouteContent, "parent/:child");
      TestRenderer.create(<TestRoute/>);

    });

    expect(paramsReceived).not.toEqual(undefined);
    expect(paramsReceived).toEqual({
      params: {
        child: "1"
      }
    });

  });

  it("passes empty object in case there are not matching params", async () => {

    getCurrentPathMock.mockReturnValue("parent");

    const paramsReceived = await new Promise(resolve => {

      const ParamsReceivingRouteContent = createRouteContent(resolve);

      const TestRoute = routeWithPath(ParamsReceivingRouteContent, "parent");
      TestRenderer.create(<TestRoute/>);

    });

    expect(paramsReceived).not.toEqual(undefined);
    expect(paramsReceived).toEqual({ params: {}});

  });

});
