import { Observable } from "rxjs/index";

export type RouterServices = {
  path: {
    getCurrentPath: () => string;
    pathObservable: Observable<string>;
  },
  navigation: {
    navigateToPath: (path: string) => void;
  }
}
