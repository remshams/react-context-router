import * as React from "react";
import * as TestRenderer from "react-test-renderer";
import { RouterContext } from "../router_context";
import Router from "../router";
import { createContextAwareComponent, createTestRouterContext } from "../__test_mocks__/router";
import { from, Observable } from "rxjs";

describe("Router", () => {


  const testRouterContext = createTestRouterContext();
  const getCurrentPathMock = testRouterContext.services.path.getCurrentPath as jest.Mock;


  it("sets router data in router context", async () => {

    const routerDataExpected: RouterContext = {
      ...testRouterContext,
      router: {
        ...testRouterContext.router,
        data: {
          ...testRouterContext.router.data,
          currentPath: "parent/child",
          pathElements: ["parent", "child"]
        }
      }
    };

    getCurrentPathMock.mockReturnValue(routerDataExpected.router.data.currentPath);

    const routerContext: RouterContext = await new Observable<RouterContext>(observer => {

      const TestComponent = createContextAwareComponent(observer);

      const root = TestRenderer.create(
        <Router routerContext={testRouterContext}>
          <TestComponent/>
        </Router>

      );

      root.unmount()

    }).toPromise();

    expect(routerContext.router.data).toEqual(routerDataExpected.router.data);


  });


  it("updates router data on window location change", (done) => {

    const routerDataExpected: RouterContext = {
      ...testRouterContext,
      router: {
        ...testRouterContext.router,
        data: {
          ...testRouterContext.router.data,
          currentPath: "parent/child",
          pathElements: ["parent", "child"]
        }
      }
    };

    const pathUpdateConsumer = jest.fn();

    const context = createTestRouterContext(from(["first/path", "second/path", routerDataExpected.router.data.currentPath]));

    (context.services.path.getCurrentPath as jest.Mock).mockReturnValue("/invalid/path/");

    new Observable<RouterContext>(observer => {

      const TestComponent = createContextAwareComponent(observer);

      const root = TestRenderer.create(
        <Router routerContext={context}>
          <TestComponent/>
        </Router>

      );

      root.unmount()

    })
      .subscribe(
        routerContext => {
          pathUpdateConsumer(routerContext.router.data.currentPath);
        console.log(routerContext.router.data.currentPath);
      },
        () => {},
        () => {
          expect(pathUpdateConsumer).toHaveBeenCalledTimes(2);
          expect(pathUpdateConsumer).toHaveBeenCalledWith("/invalid/path/");
          expect(pathUpdateConsumer).toHaveBeenCalledWith(routerDataExpected.router.data.currentPath);
          done();
        }
        );
  });


});

