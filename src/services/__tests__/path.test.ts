import {
  areContextElementsInPath,
  extractPathElements,
  extractPathElementsOnRouteContext,
  RouteElement,
  stripPathElementsOnRouteContext
} from "../path";

describe("Routing", () => {


  describe("extractPathElements", () => {

    it("extracts elements from path with single element", () => {

      const path = "/test";

      const elementsExpected = ["test"];

      const elementsReturned = extractPathElements(path);

      expect(elementsReturned).toEqual(elementsExpected);

    });

    it("extracts elements from path with no leading slash", () => {

      const path = "test";

      const elementsExpected = ["test"];

      const elementsReturned = extractPathElements(path);

      expect(elementsReturned).toEqual(elementsExpected);

    });

    it("extracts elements from path with no trailing slash", () => {

      const path = "test/";

      const elementsExpected = ["test"];

      const elementsReturned = extractPathElements(path);

      expect(elementsReturned).toEqual(elementsExpected);

    });

    it("extracts elements from path with multiple elements", () => {

      const path = "/test/1234";

      const elementsExpected = ["test", "1234"];

      const elementsReturned = extractPathElements(path);

      expect(elementsReturned).toEqual(elementsExpected);

    });

    it("returns empty path elements in case path is not set", () => {

      const elementsReturned = extractPathElements();

      expect(elementsReturned).toEqual([]);

    });


  });

  describe("stripPathElementsOnRouteContext", () => {

    it("removes route elements before route context", () => {

      const context = ["test"];
      const routeElements: Array<RouteElement> = ["test", "sub1", "sub2"];

      expect(stripPathElementsOnRouteContext(context, routeElements)).toEqual(routeElements)
    });

    it("supports path with nested route context", () => {

      const context = ["parent", "child", "sub1", "sub2"];
      const routeElements: Array<RouteElement> = ["test", "parent", "child"];

      expect(stripPathElementsOnRouteContext(context, routeElements)).toEqual(routeElements.slice(1))

    });

    it("supports path with empty elements", () => {

      const context = ["parent", "", "child", "sub1", "sub2"];
      const routeElements: Array<RouteElement> = ["test", "parent", "child"];

      expect(stripPathElementsOnRouteContext(context, routeElements)).toEqual(routeElements.slice(1))

    });

    it("returns null in case context is empty", () => {

      const context: Array<RouteElement> = [];
      const routeElements: Array<RouteElement> = ["test", "parent", "child"];

      expect(stripPathElementsOnRouteContext(context, routeElements)).toEqual(null)

    });

    it("returns null in case route context cannot be found", () => {

      const context = ["test"];
      const routeElements: Array<RouteElement> = ["whatever"];

      expect(stripPathElementsOnRouteContext(context, routeElements)).toEqual(null)
    });

  });

  describe("extractPathElementsOnRouteContext", () => {


    it("returns route elements based on context", () => {

      const context = "test";
      const path = `${context}/realm/123456`;

      const routeElementsExpected = [context, "realm", "123456"];

      expect(extractPathElementsOnRouteContext(context, path)).toEqual(routeElementsExpected)

    });

    it("returns null in case context cannot be found", () => {

      const context = "test";
      const path = "other";


      expect(extractPathElementsOnRouteContext(context, path)).toEqual(null)

    });

  });

  describe("areContextElementsInPath", () => {

    it("returns true in case single route element is in path", () => {

      const pathElements = ["first", "parent", "1", "child"];

      const routeElement = ["child"];

      expect(areContextElementsInPath(routeElement, pathElements)).toEqual(true);

    });

    it("returns true in case route elements are at the beginning", () => {

      const pathElements = ["first", "parent", "1", "child", "1"];

      const routeElement = ["first", ":parent", ":parentId"];

      expect(areContextElementsInPath(routeElement, pathElements)).toEqual(true);

    });

    it("returns true in case route elements are at the end", () => {

      const pathElements = ["first", "parent", "1", "child", "1"];

      const routeElement = ["child", ":childId"];

      expect(areContextElementsInPath(routeElement, pathElements)).toEqual(true);

    });

    it("returns true in case route elements are in the middle", () => {

      const pathElements = ["first", "parent", "1", "child", "1"];

      const routeElement = ["parent", ":parentId", "child"];

      expect(areContextElementsInPath(routeElement, pathElements)).toEqual(true);

    });

    it("returns true in case last route element is a parameter and path elements are shorter by one", () => {

      const pathElements = ["parent"];

      const routeElement = ["parent", ":parentId"];

      expect(areContextElementsInPath(routeElement, pathElements)).toEqual(true);

    });

    it("returns false in case one of the route elements is not in path", () => {

      const pathElements = ["first", "parent", "1", "child", "1"];

      const routeElement = ["notMatching", ":childId"];

      expect(areContextElementsInPath(routeElement, pathElements)).toEqual(false);

    });

    it("returns false in case one of the route elements is a placeholder and does not match", () => {

      const pathElements = ["first", "parent", "1", "child", "1"];

      const routeElement = ["parent", "child"];

      expect(areContextElementsInPath(routeElement, pathElements)).toEqual(false);

    });

    it("returns false in case context elements are longer than path", () => {

      const pathElements = ["parent", "1"];

      const routeElement = ["parent", ":parentId", "child"];

      expect(areContextElementsInPath(routeElement, pathElements)).toEqual(false);

    });

    it("returns false in case last route element is not a parameter and path elements are shorter by one", () => {

      const pathElements = ["parent"];

      const routeElement = ["parent", "child"];

      expect(areContextElementsInPath(routeElement, pathElements)).toEqual(false);

    });

  });

});

